const Vibrant = require("node-vibrant");
const fs = require("fs");
const Jimp = require("jimp");
const child_process = require("child_process");

let v = new Vibrant('static/source/images/js/conway_original.png')

// const builder = Vibrant.from("static/source/images/js/conway_original.png");
const referenceBGPath = "static/source/images/js/conway_original.png";

const swatchJSONMap = swatch => ({
  hex: swatch.getHex(),
  hsl: swatch.getHsl()
});

const modifySwatchByLuminenceIfMax = (swatch, lumDiff, max) => {
  const l = swatch.hsl[2]
  if (l > lumDiff) {
    const rgb = Vibrant.Util.hslToRgb(swatch.hsl[0], swatch.hsl[1], swatch.hsl[2] - lumDiff)
    swatch.hex = Vibrant.Util.rgbToHex(rgb[0], rgb[1], rgb[2])
  }
  return swatch
}

v.getPalette((err, palette) => {
  // Need a lot of fallbacks :D
  const swatches = {
    vibrant: swatchJSONMap(palette.Vibrant || palette.LightVibrant || palette.DarkVibrant),
    "vibrant-light": swatchJSONMap(palette.LightVibrant || palette.Vibrant || palette.DarkVibrant),
    "vibrant-dark": swatchJSONMap(palette.DarkVibrant || palette.Vibrant || palette.LightVibrant),
    muted: swatchJSONMap(palette.Muted || palette.LightMuted || palette.DarkMuted),
    "muted-light": swatchJSONMap(palette.LightMuted || palette.Muted || palette.DarkMuted),
    "muted-dark": swatchJSONMap(palette.DarkMuted || palette.Muted || palette.LightMuted)
  }

  // Ensure a max lightness: https://gitlab.com/danger-systems/danger.systems/issues/112
  swatches["muted"] = modifySwatchByLuminenceIfMax(swatches["muted"], 0.2, 0.8)
  swatches["muted-light"] = modifySwatchByLuminenceIfMax(swatches["muted-light"], 0.05, 0.8)
  swatches["muted-dark"] = modifySwatchByLuminenceIfMax(swatches["muted-dark"], 0.25, 0.8)

  writeJSON(swatches);
  writeSCSS(swatches);
  updateSVGs(swatches);
  writeLogos(swatches);
});

const writeJSON = swatches => {
  const jsonPath = "static/json_data/js_color_scheme.js";
  fs.writeFileSync(jsonPath, JSON.stringify(swatches));
};

const writeSCSS = swatches => {
  const scssPath = "static/source/stylesheets/colors-js.scss";
  const scss = Object.keys(swatches).map(
    key => `$${key}: ${swatches[key].hex};`
  );
  fs.writeFileSync(scssPath, scss.join("\n"));
};

const updateSVGs = swatches => {
  const originalCSSHex = "#F0D34B";
  const before = "static/source/images/js/_before.svg";
  const beforeRender = "static/source/images/js/before.svg";
  const after = "static/source/images/js/_after.svg";
  const afterRender = "static/source/images/js/after.svg";

  const updateFile = (file, newFile, oldColor, newColor) => {
    const original = fs.readFileSync(file, "utf8");
    const modified = original.replace(RegExp(oldColor, "g"), newColor);
    fs.writeFileSync(newFile, modified);
  };
  updateFile(before, beforeRender, originalCSSHex, swatches["muted-light"].hex);
  updateFile(after, afterRender, originalCSSHex, swatches["muted"].hex);
};

const writeLogos = swatches => {
  const largeMaskPath = "static/source/images/js/masks/danger-logo-mask-hero@2x.png";
  const heroImagePath = "static/source/images/js/danger-logo-hero@2x.png";
  const smallLogoPath = "static/source/images/js/danger-logo-small@2x.png";

  Promise.all([Jimp.read(referenceBGPath), Jimp.read(largeMaskPath)])
    .then(([bg, mask, js]) => {
      bg
        .cover(mask.bitmap.width, mask.bitmap.height)
        .mask(mask, 0, 0)
        .crop(0, 0, mask.bitmap.width, mask.bitmap.height)
        .write(heroImagePath)
        .resize(252, 80)
        .write(smallLogoPath)

    })        .then( () => {
          compressPNG(heroImagePath)
          compressPNG(smallLogoPath)
        })


    .catch(function(err) {
      console.error(err);
    });

  const jsRenderedPath = "static/source/images/home/js-logo@2x.png"
  const jsMaskPath = "static/source/images/js/masks/js-logo-mask@2x.png"
  Promise.all([Jimp.read(referenceBGPath), Jimp.read(jsMaskPath)])
    .then(([bg, mask]) => {
      bg
        .cover(mask.bitmap.width, mask.bitmap.height)
        .mask(mask, 0, 0)
        .crop(0, 0, mask.bitmap.width, mask.bitmap.height)
        .write(jsRenderedPath)
      
    }).then(() => {
          compressPNG(jsRenderedPath)
    })

    .catch(function(err) {
      console.error(err);
    });

};

const compressPNG = file => {
  try {
    child_process.execSync(`pngquant ${file} -f --speed 1 --output ${file}`);
  } catch (error) {
    console.log("No `pngquant` installed, skipping minification.");
    console.error(error)
  }
};
